# Predicting outcomes after intradetrusor onabotulinumtoxina for non-neurogenic urgency incontinence in women 

Whitney K Hendrickson, Gongbo Xie, David D Rahn, Cindy L Amundsen, James A Hokanson, Megan Bradley, Ariana L Smith, Vivian W Sung, Anthony G Visco, Sheng Luo, J Eric Jelovsek.

PMID: 34859485 PMCID: PMC9014828 (available on 2023-01-01) DOI: 10.1002/nau.24845

This site provides information and source code for models in the above project.

This research was funded by Duke University, the Society of Urodynamics, Female Pelvic Medicine and Urogenital Reconstruction Foundation, and the NICHD.

The copyrights of this software are owned by Duke University. As such, two licenses to this software are offered:
(1) An open-source license under the GPLv2 license.
(2) A custom license with Duke University, for use without the GPLv2 restrictions.

As a recipient of this software, you may choose which license to receive the code under. Outside contributions to the Duke owned code base cannot be accepted unless the contributor transfers the copyright to those changes over to Duke University.
To enter a license agreement without the GPLv2 restrictions, please contact the Digital Innovations department at Duke Office of Licensing and Ventures (https://olv.duke.edu/software/) at olvquestions@duke.edu with reference to “OLV File No. 7484” in your email.

Please note that this software is distributed AS IS, WITHOUT ANY WARRANTY; and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
